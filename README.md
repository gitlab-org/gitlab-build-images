# GitLab Build Images

This repository is used to build docker images that are used to build and test
various parts of GitLab:

1. Build Omnibus packages
1. Test GitLab-CE/EE project
1. Build gitlab-org/www-gitlab-com project

## Adding a new image

There are two methods to adding a new image:

1. Use a Dockerfile.
1. Use a custom image with versioned features.

### Use a Dockerfile

A bespoke image is one that uses it's own Dockerfile. In the interests of
avoiding repetition, it's better to use the custom docker build if possible.

Images from a custom Dockerfile are built from job definitions in [custom.images.yml](.gitlab/ci/custom.images.yml)

Assuming the image you want to add is called `new-image`:

1. Add a new build job:

   ```yml
   new-image:
     extends: .build_static_image
   ```

   **NOTE:** You can use `variables` keyword to build images for multiple architectures (the default architecture is `linux/amd64`):

   ```yml
   new-image:
     extends: .build_static_image
     variables:
       PLATFORM: "linux/arm64,linux/amd64"
   ```

1. Add a Dockerfile: `Dockerfile.new-image`.

### Use a custom image with versioned features

To add a new image to the build, create a new images definition file like [`gitlab.images.yml`](.gitlab/ci/gitlab.images.yml).

With custom images, the components and versions define the resulting image name and tag.

Major components define image path. Base image is defined by `OS` component. Supported base images are:

1. `debian:$version`
1. `ubi:$version`

[Supported major](https://gitlab.com/gitlab-org/gitlab-build-images/-/blob/master/scripts/lib/custom-docker.sh#L4) components are:

1. `OS`
1. `RUBY`
1. `GOLANG`
1. `RUST`
1. `NODE`
1. `POSTGRES`

Minor components define tag name. [Supported minor](https://gitlab.com/gitlab-org/gitlab-build-images/-/blob/master/scripts/lib/custom-docker.sh#L5) components are:

1. `RUBYGEMS`
1. `GIT`
1. `LFS`
1. `CHROME`
1. `YARN`
1. `GRAPHICSMAGICK`
1. `EXIFTOOL`
1. `BAZELISK`
1. `DOCKER`
1. `BUILDX`
1. `GCLOUD`
1. `KUBECTL`
1. `HELM`

The names are defined as `[feature]-[version]` pairs, separated by `-`.

Here are some example build names:

1. `debian-bullseye-ruby-3.2.4-golang-1.15:git-2.29`
1. `debian-bullseye-ruby-3.2.4-node-14.15:git-2.29-lfs-2.9-yarn-1.22-graphicsmagick-1.3.34`
1. `debian-bullseye-ruby-3.2.4-golang-1.14-postgresql-12:git-2.29-lfs-2.9-chrome-87-node-14.15-yarn-1.22-graphicsmagick-1.3.34`

#### Adding a new build

As an example, if you want to add new image for Ruby 3.2.4 with `git` 2.29, `golang` 1.15, and `rust` 1.73.0 based on `debian` bookworm,
following job definition would have to be created:

```yml
new_image:
  extends:
    - .build_dynamic_image
  variables:
    OS: 'debian:bookworm'
    RUBY: '3.2.4'
    GOLANG: '1.15'
    RUST: '1.73.0'
    GIT: '2.29'
```

Note that tag name is limited to 128 characters so amount of `minor components` an image can have is limited by it.

#### Pushing a rebuilt image

You might need to run a manual job to build and push a rebuilt image after a merge request is merged.

## Note regarding Google Chrome

Google has a policy of yanking older versions of Google Chrome from their PPA
whenever a new major version is released. To help maintain consistent build
images, there is a CI step that saves the latest Google Chrome and Chromium .deb into an the
GitLab package registry. The install for Chrome will use registry to fetch corresponding packages.
Make sure a certain version is cached before updating it in build jobs.
See `scripts/cache-google-chrome` for more details.

## Note regarding Golang and FIPS

When adding support for a new major/minor Golang version, we also need to
ensure there's a matching [`golang-fips`](https://github.com/golang-fips/)
branch available, e.g. for Golang 1.21 there's https://github.com/golang-fips/go/tree/go1.21-fips-release.
The `golang-fips` branch creation process can take time and so it
[may be necessary to use the previous version](https://gitlab.com/gitlab-org/gitlab-build-images/-/merge_requests/771)
until it's available.

## Linting

We use `shfmt` for formating Bash scripts, to run against all Bash scripts within the repo,

```bash
find . -type f -exec grep -l "^#! */bin/bash" {} + | xargs shfmt -i 2 -w
```

## Contributing

See [Contributing](CONTRIBUTING.md)
